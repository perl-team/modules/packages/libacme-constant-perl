Source: libacme-constant-perl
Maintainer: Debian Perl Group <pkg-perl-maintainers@lists.alioth.debian.org>
Section: perl
Testsuite: autopkgtest-pkg-perl
Priority: optional
Build-Depends: debhelper-compat (= 13)
Build-Depends-Indep: libbareword-filehandles-perl <!nocheck>,
                     libindirect-perl <!nocheck>,
                     libmultidimensional-perl <!nocheck>,
                     libstrictures-perl <!nocheck>,
                     perl
Standards-Version: 4.6.1
Vcs-Browser: https://salsa.debian.org/perl-team/modules/packages/libacme-constant-perl
Vcs-Git: https://salsa.debian.org/perl-team/modules/packages/libacme-constant-perl.git
Homepage: https://metacpan.org/release/Acme-constant
Rules-Requires-Root: no

Package: libacme-constant-perl
Architecture: all
Depends: ${misc:Depends},
         ${perl:Depends},
         libstrictures-perl
Description: module that makes inconstant constants, except actually not
 Acme::constant lets you make inconstant constants, just like the constants the
 users of Ruby or Opera (before Opera 14, that is) already enjoyed.
 .
 Unlike Perl constants, that are replaced at compile time, Acme constants, in
 true dynamic programming language style, can be modified even after
 declaration.
 .
 Just like constants generated with standard use constant pragma, the
 constants declared with use Acme::Constant don't have any sigils. This makes
 using constants easier, as you don't have to remember what sigil do constants
 use.
